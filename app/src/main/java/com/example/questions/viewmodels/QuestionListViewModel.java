package com.example.questions.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.questions.database.questions.Question;
import com.example.questions.database.questions.QuestionRepository;
import com.example.questions.database.questions.QuestionWithAnswers;

import java.util.List;

public class QuestionListViewModel extends AndroidViewModel {
    private QuestionRepository questionRepository;

    public QuestionListViewModel(Application application) {
        super(application);
        this.questionRepository = new QuestionRepository(application);
    }

    public LiveData<List<Question>> getQuestions() {
        return questionRepository.getQuestions();
    }

    public LiveData<QuestionWithAnswers> getQuestionWithAnswersById(Long id) {
        return questionRepository.getQuestionWithAnswersById(id);
    }

    public void insert(Question question) {
        questionRepository.insert(question);
    }
}

package com.example.questions.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.questions.database.answers.Answer;
import com.example.questions.database.answers.AnswerRepository;

import java.util.List;

public class AnswerListViewModel extends AndroidViewModel {
    private AnswerRepository answerRepository;

    public AnswerListViewModel(Application application) {
        super(application);
        this.answerRepository = new AnswerRepository(application);
    }

    public LiveData<List<Answer>> getAnswers() {
        return answerRepository.getAnswers();
    }

    public void insert(Answer answer) {
        answerRepository.insert(answer);
    }

    public void delete(Answer answer) {
        answerRepository.delete(answer);
    }
}
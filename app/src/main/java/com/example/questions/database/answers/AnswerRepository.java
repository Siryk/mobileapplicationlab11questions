package com.example.questions.database.answers;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.questions.database.AppDatabase;

import java.util.List;

public class AnswerRepository {
    private AnswerDao answerDao;

    public AnswerRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        this.answerDao = db.answerDao();
    }

    public LiveData<List<Answer>> getAnswers() {
        return answerDao.getAnswers();
    }

    public void insert(Answer answer) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            answerDao.insert(answer);
        });
    }

    public void delete(Answer answer) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            answerDao.delete(answer);
        });
    }
}

package com.example.questions.database.questions;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface QuestionDao {
    @Query("SELECT * FROM questions")
    LiveData<List<Question>> getQuestions();

    @Query("SELECT * FROM questions WHERE id = :questionId LIMIT 1")
    LiveData<QuestionWithAnswers> getQuestionWithAnswersById(Long questionId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Question word);
}

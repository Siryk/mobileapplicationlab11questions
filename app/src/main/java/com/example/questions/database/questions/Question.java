package com.example.questions.database.questions;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "questions")
public class Question {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String question;

    public Question(@NonNull String question) {
        this.question = question;
    }
}

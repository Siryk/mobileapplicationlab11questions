package com.example.questions.database.questions;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.questions.database.AppDatabase;

import java.util.List;

public class QuestionRepository {
    private QuestionDao questionDao;

    public QuestionRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        this.questionDao = db.questionDao();
    }

    public LiveData<List<Question>> getQuestions() {
        return questionDao.getQuestions();
    }

    public LiveData<QuestionWithAnswers> getQuestionWithAnswersById(Long id) {
        return questionDao.getQuestionWithAnswersById(id);
    }

    public void insert(Question question) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            questionDao.insert(question);
        });
    }
}

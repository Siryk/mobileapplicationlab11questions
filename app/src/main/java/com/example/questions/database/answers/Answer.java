package com.example.questions.database.answers;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.questions.database.questions.Question;

@Entity(tableName = "answers",
        foreignKeys = @ForeignKey(entity = Question.class, parentColumns = "id", childColumns = "question_id"),
        indices = @Index("question_id")
)
public class Answer {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String answer;

    @ColumnInfo(name = "question_id")
    public long questionId;

    public Answer(@NonNull String answer, long questionId) {
        this.answer = answer;
        this.questionId = questionId;
    }
}

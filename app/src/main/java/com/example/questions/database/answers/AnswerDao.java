package com.example.questions.database.answers;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AnswerDao {
    @Query("SELECT * FROM answers")
    LiveData<List<Answer>> getAnswers();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Answer word);

    @Delete
    void delete(Answer answer);
}

package com.example.questions.database.questions;

import static java.util.Collections.emptyList;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.questions.database.answers.Answer;

import java.util.List;

public class QuestionWithAnswers {
    @Embedded
    public Question question;

    @Relation(parentColumn = "id", entityColumn = "question_id")
    public List<Answer> answers = emptyList();
}

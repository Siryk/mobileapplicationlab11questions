package com.example.questions;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.questions.adapters.QuestionAdapter;
import com.example.questions.database.questions.Question;
import com.example.questions.viewmodels.QuestionListViewModel;

public class MainActivity extends AppCompatActivity {
    private RecyclerView questionRecyclerView;
    private QuestionListViewModel questionListViewModel;
    private QuestionAdapter questionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questionRecyclerView = findViewById(R.id.recyclerView);

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(view -> openInputDialog());

        setupQuestionRecyclerView();
    }

    private void openInputDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_input, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view)
                .setPositiveButton(R.string.ok_button, (dialog, which) -> {
                    EditText editText = view.findViewById(R.id.editText);
                    String inputText = editText.getText().toString();
                    questionListViewModel.insert(new Question(inputText));
                })
                .setNegativeButton(R.string.cancel_button, null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setupQuestionRecyclerView() {
        questionListViewModel = new ViewModelProvider(this).get(QuestionListViewModel.class);
        questionAdapter = new QuestionAdapter(this);

        questionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        questionRecyclerView.setAdapter(questionAdapter);

        questionListViewModel.getQuestions().observe(this, questions -> {
            questionAdapter.setQuestions(questions);
        });
    }
}

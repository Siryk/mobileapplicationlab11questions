package com.example.questions;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.questions.adapters.AnswerAdapter;
import com.example.questions.database.answers.Answer;
import com.example.questions.viewmodels.AnswerListViewModel;
import com.example.questions.viewmodels.QuestionListViewModel;

public class QuestionAnswersActivity extends AppCompatActivity {
    private RecyclerView answerRecyclerView;
    private AnswerListViewModel answerListViewModel;
    private QuestionListViewModel questionListViewModel;
    private TextView questionTextView;
    private AnswerAdapter answerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_answers);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        answerRecyclerView = findViewById(R.id.recyclerView);
        questionTextView = findViewById(R.id.questionTextView);
        Button addButton = findViewById(R.id.buttonAddItem);

        Long questionId = getIntent().getLongExtra("questionId", 0);

        addButton.setOnClickListener(view -> {
            String inputText = getInputText();
            answerListViewModel.insert(new Answer(inputText, questionId));

        });

        setupViewModels();
        setupRecyclerView();
        observeQuestionWithAnswers(questionId);
    }

    private String getInputText() {
        EditText editText = findViewById(R.id.editTextItem);
        return editText.getText().toString();
    }

    private void setupViewModels() {
        ViewModelProvider viewModelProvider = new ViewModelProvider(this);
        answerListViewModel = viewModelProvider.get(AnswerListViewModel.class);
        questionListViewModel = viewModelProvider.get(QuestionListViewModel.class);
    }

    private void setupRecyclerView() {
        answerAdapter = new AnswerAdapter(this, answerListViewModel);
        answerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        answerRecyclerView.setAdapter(answerAdapter);
    }

    private void observeQuestionWithAnswers(long questionId) {
        questionListViewModel.getQuestionWithAnswersById(questionId)
                .observe(this, questionWithAnswers -> {
                    answerAdapter.setAnswers(questionWithAnswers.answers);
                    questionTextView.setText(questionWithAnswers.question.question);
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
